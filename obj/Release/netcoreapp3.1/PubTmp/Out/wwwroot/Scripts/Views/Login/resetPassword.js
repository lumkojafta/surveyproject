﻿$(document).ready(function () {
    $('#newPassword').keyup(function () {
        $('#strengthMessage').html(checkStrength($('#newPassword').val()))
    })
    function checkStrength(password) {
        var strength = 0
        if (password.length < 6) {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Short')
            return 'Too short'
        }
        if (password.length > 7) strength += 1
        // If password contains both lower and uppercase characters, increase strength value.  
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
        // If it has numbers and characters, increase strength value.  
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
        // If it has one special character, increase strength value.  
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
        // If it has two special characters, increase strength value.  
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
        // Calculated strength value, we can return messages  
        // If value is less than 2  
        if (strength < 2) {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Weak')
            return 'Weak'
        } else if (strength == 2) {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Good')
            return 'Good'
        } else {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Strong')
            return 'Strong'
        }
    }
});

function resetPassword() {
    var newPassword = $('#newPassword').val();
    var password = btoa(newPassword);
    const urlParams = window.location.search;

    var validate = validatePage();
    var confirmPwd = confirmPassword();

    
    if (validate) {
        if ($('#strengthMessage').html() === 'Strong' || $('#strengthMessage').html() === 'Strong') {
            if (confirmPwd) {
                $.ajax({
                    method: "GET",
                    url: '/Login/SaveNewPassword' + urlParams,
                    contentType: "application/json; charset=utf-8",
                    data: {
                        newPassword: newPassword,
                    },
                    cache: false
                })
                    .done(function (data) {
                        swal({
                            title: 'Successfull!',
                            text: 'Please Proceed To Login',
                            icon: 'success',
                            button: {
                                text: "Continue",
                                value: true,
                                visible: true,
                                className: "btn btn-primary"
                            }
                        })

                    })
                    .fail(function (ajaxRequest, status, error) {
                        console.warn('ajaxRequest: ', ajaxRequest);
                        console.warn('status: ', status);
                        console.warn('error: ', error);
                    });
            }
        } else {
            $('#newPassword').parent().css('border', 'red 2px solid');
        }
    }
}

function confirmPassword() {
    if ($('#newPassword').val() != $('#passwordConfirm').val()) {
        $('#divPasswordError').css('display', 'block');
        $('#newPassword').parent().css('border', 'red 2px solid');
        $('#passwordConfirm').parent().css('border', 'red 2px solid');
        return false;
    } else {
        $('#divPasswordError').css('display', 'none');
        $('#passwordConfirm').parent().css('border', 'transparent 0px solid');
        $('#newPassword').parent().css('border', 'transparent 0px solid');
       
    }
    return true;
}

function validatePage() {
    if ($('#newPassword').val() === '') {
        $('#newPassword').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#newPassword').parent().css('border', 'transparent 0px solid');

    }
    if ($('#passwordConfirm').val() === '') {
        $('#passwordConfirm').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#passwordConfirm').parent().css('border', 'transparent 0px solid');

    }
    return true;
}


function showNewPassword() {
    var x = document.getElementById("newPassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function showConfirmPassword() {
    var x = document.getElementById("passwordConfirm");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}