﻿var addClientTable;
var myArray;
$(document).ready(function () {
    Init();
    getCountries();
    getAccountManager();
   
});
function Init() {
    //create table
    addClientTable = $('#addClientTable').DataTable({
        "ajax": {
            "method": 'GET',
            "url": '/Client/GetClients',
            "dataSrc": '',
        },
        pagingType: 'full_numbers',
        "columns": [
            {
                'title': 'Name',
                "data": "name"
            },
            {
                'title': 'Country',
                "data": "country"
            },
            {
                'title': 'DepotCode',
                "data": "depotCode"
            },
            {
                'title': 'ProductName',
                "data": "productName"
            },
            {
                'title': 'BillingCurrency',
                "data": "billingCurrency"
            },
            {
                'title': 'BillingDate',
                "data": "billingDate"

            },
            {
                'title': 'Sla',
                "data": "sla"
            },
            {
                'title': 'Edit',
                render: function (data, type, row) {
                    return '<button id="' + row.clientId + '" onclick="updateClient(' + row.clientId + ', \'' + row.country + '\', \'' + row.depotCode + '\'); return false;" class="btn btn-outline-primary">edit</button>';

                }
            },
            {
                'title': 'Finaces',
                render: function (data, type, row) {
                    return '<button id="' + row.clientId + '" onclick="return false;" class="btn btn-outline-primary">finances</button>';

                }
            }
        ],
    });
    $('#addClientTable').each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });
};
function AddNewClient() {

    clearFields();
    populateCounties();
    populateProducts();
    getAccountManager();
    //$('#drpDepots').addClass('multiSelectDepots');
    //$('#drpProducts').addClass('multiSelectProducts');
    $('#drpDepots').select2({
        placeholder: 'Select an option',
        allowClear: true,
    });
    $('#drpProducts').select2({
        placeholder: 'Select an option',
        allowClear: true,
    });
}

function clearFields() {
    $('#divAddClient').show();
    $('#divTableContainer').hide();
    $("#divAddMultiSelect").show();
    $("#txtSLADetails").val("");
    $("#drpBillingDate").val("");
    $("#drpBillingCurrency").val("");
    $("#drpSupportRate").val("");
    $("#drpAccountManager").val("");
    $("#drpProducts").val("");
    $("#txtDepot").val("");
    $("#drpDepots").val("");
    $("#drpCountries").val("");
    $("#txtName").val("");
    $("#drpSLA").val("");
}
function cancel() {
    $('#divAddClient').hide();
    $('#divTableContainer').show();
}


$("#drpSupportRate").blur(function () {
    var $this = $(this); $this.val(parseFloat($this.val().replace(/[^\d.]/g, '')).toFixed(2));
});


function saveClient() {
    isModalValid = validate();
    if (isModalValid) {
        let id = $('#clientId').val();
        let name = $("#txtName").val();
        let country = $("#drpCountries :selected").text();    
        let depotCode = $("#drpDepots").val();
        let productName = $("#drpProducts").val();        
        let accountManger = $("#drpAccountManager :selected").text();
        let supportRate = $("#drpSupportRate").val();
        let billingCurrency = $("#drpBillingCurrency :selected").text();
        let billingDate = $("#drpBillingDate :selected").text();
        let sla = $("#drpSLA :selected").text();
        let sladetails = $('#txtSLADetails').val();
        let countryId = $("#drpCountries :selected").val();       
        let model = {

            ClientId: id, Name: name, Country: country, DepotCode: depotCode, ProductName: productName, AccountManger: accountManger, SupportRate: supportRate, BillingCurrency: billingCurrency,
            Sla: sla, Sladetails: sladetails, BillingDate: billingDate, CountryId: countryId
        };
        console.log("model", model);
        $.ajax({
            type: "POST",
            url: "/Client/SaveClient",
            data: model,
            dataType: "json",
            success: function (data) {
                
                if (data.isSuccess == false) {
                    //alert(data.errorMsg);
                    //clearFields();
                    //window.location.reload();
                    swal({
                        title: 'Error!',
                        text: model.Name + ' already exists as a client name for the selected country.',
                        icon: 'error',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    }).then(function () {
                        //clearFields();
                        //window.location.reload();
                    });

                } else {
                    // this is actually a success message
                    swal({
                        title: 'Success!',
                        text: 'You have successfully added a new Client',
                        icon: 'success',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    }).then(function () {
                        clearFields();
                        window.location.reload();
                    });
                }
              
            },
            error: function (data) {
                if (data.isSuccess == false) {
                    alert(data.errorMsg);
                }
            }
        })
    }
}
function updateClient(clientId, countryName, depotCode) {

    var id = parseInt(clientId);
    $('#clientId').val(id);
    var data = { 'clientId': id, 'countryName': countryName, 'depotCode': depotCode }
    //$("#clientId").val(id);
    //var clientId = { 'clientId': id }
    $('#divAddClient').show();
    $('#divTableContainer').hide();
    $.ajax({
        method: "GET",
        url: '/Client/EditClient',
        dataType: "json",
        data: data,
        cache: false
    })
        .done(function (data) {
            clearFields();

            $('#drpDepots').select2({
                placeholder: 'Select an option',
                allowClear: true,
            });
            $('#drpProducts').select2({
                placeholder: 'Select an option',
                allowClear: true,
            });
            // $('#divEditSupportCase').css('display', 'block');   

            $("#txtName").val(data.name);
            $("#divAddMultiSelect").show();
            $("#drpCountries").empty();
            getCountry(data.countryId);
            $("#drpCountries").append('<option  id="' + data.countryId + '" value="' + data.countryId + '">' + data.country + '</option>');
            $("#drpCountries :selected").text(data.country);

            $("#drpAccountManager").empty();
            getAccountManager();

            $("#drpProducts").empty();
            for (var x = 0; x < data.allProducts.length; x++) {
                $("#drpProducts").append('<option  id="' + data.allProducts[x] + '" value="' + data.allProducts[x] + '">' + data.allProducts[x] + '</option>');
            }
            $("#drpProducts").val(data.productName);
            $("#drpProducts").trigger('change');


            $("#drpDepots").empty();
            for (var x = 0; x < data.allDepotCodes.length; x++) {
                $("#drpDepots").append('<option  id="' + data.allDepotCodes[x] + '" value="' + data.allDepotCodes[x] + '">' + data.allDepotCodes[x] + '</option>');
            }
            $("#drpDepots").val(data.depotCode);
            $("#drpDepots").trigger('change');

            $("#drpSupportRate").val(data.supportRate);

            $("#drpBillingCurrency").empty();            
            const values = [];
            const myArray2 = ['ZAR', 'NGN', 'EUR', 'Z$', 'SZL', 'BWP', 'TZS'];
            const drpBillingCurrency = $("#drpBillingCurrency");
            drpBillingCurrency.append(`<option value="${data.billingCurrency}">${data.billingCurrency}</option>`);
            values.push(data.billingCurrency);
            for (let i = 0; i < myArray2.length; i++) {
                if (myArray2[i] === data.billingCurrency) {
                    continue;
                }
                const value = myArray2[i];
                drpBillingCurrency.append(`<option value="${value}">${value}</option>`);               
            }

            $("#drpBillingCurrency :selected").text(data.billingCurrency);         

            $("#drpBillingDate").empty();
            const intities = [];
            const myArray3 = Array.from({ length: 31 }, (_, i) => i + 1);      
            const drpBillingDate = $("#drpBillingDate");
            drpBillingDate.append(`<option value="${data.billingDate}">${data.billingDate}</option>`);
            intities.push(data.billingDate);
            for (let i = 0; i < myArray3.length; i++) {
                if (myArray3[i] === data.billingDate) {
                    continue;
                }
                const value = myArray3[i];               
                drpBillingDate.append(`<option value="${value}">${value}</option>`);                 
            }      ;
            $("#drpBillingDate").val(data.billingDate);

            $("#drpSLA").empty();        
            const myArray = [true, false];                      
            const $drpSLA = $("#drpSLA");           
            // add selected value as first option
            $drpSLA.append(`<option value="${data.sla}">${data.sla}</option>`);
            // add remaining options
            for (let i = 0; i < myArray.length; i++) {
                if (myArray[i] === data.sla) continue; // skip selected index
                $drpSLA.append(`<option value="${myArray[i]}">${myArray[i]}</option>`);               
            }
            $("#drpSLA :selected").text(data.sla);

            $("#txtSLADetails").val(data.sladetails);
        })
        .fail(function (ajaxRequest, status, error) {
        })
        .always(function () {
        });
};

function populateCounties() {
    $.ajax({
        type: "GET",
        url: "/Client/GetCountries",
        dataType: "json",
        success: function (data) {
            $("#drpCountries").empty();
            $("#drpCountries").append('<option  value="-1" > -- Select A Country -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
            }
        },
        error: function (data) {
        }
    })
}

function getDepot(countrID) {
    $.ajax({
        type: "GET",
        url: "/User/GetDepots",
        dataType: "json",
        success: function (data) {
            var index = -1;
            for (var x = 0; x < data.length; x++) {
                if (data[x].countryId == countrID) {
                    index = x;
                    break;
                }
            }
            if (index != -1) {
                $("#drpDepots").append('<option  id="' + data[index].countryId + '" value="' + data[index].countryId + '">' + data[index].depotCode + '</option>');
            };
            /*$("#drpRoles").append('<option  value="-1" > -- Select A Role -- </option>');*/
            for (var x = 0; x < data.length; x++) {
                if (x != index) {
                    $("#drpDepots").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].depotCode + '</option>');
                }
            }
        },
        error: function (data) {
        }
    })
}
function populateDepots(countryId, callback) {

    if (countryId === undefined)
        countryId = $("#drpCountries :selected").val();

    var model = {
        CountryId: countryId
    };
    $.ajax({
        type: "POST",
        url: "/Client/GetDepots",
        data: model,
        dataType: "json",
        success: function (data) {
            $("#drpDepots").empty();
            for (var x = 0; x < data.length; x++) {
                if (data[x] !== null) {
                    $("#drpDepots").append('<option  id="depot' + data[x].depotId + '" value="' + data[x].depotCode + '">' + data[x].depotCode + '</option>');
                }
            }
            if (callback) {
                callback();
            }
        },
        error: function (data) {
        }
    })
}



function addDepot() {
    let clientId = $('#clientId').val();
    let countryId = $("#drpCountries :selected").val();
    let depotCode = $("#txtDepot").val();
    let model = {
        countryId: countryId,
        depotCode: depotCode,
        clientId: clientId
    };
    
    $.ajax({
        type: "POST",
        url: "/Client/InsertDepots",
        data: model,
        dataType: "json",
        success: function (data) {
            

            if (data.includes("Error Adding Depot")) {
                swal({
                    title: 'Error!',
                    text: data,
                    icon: 'error',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-danger"
                    }
                });
            } else {
                swal({
                    title: 'Success!',
                    text: data,
                    icon: 'success',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"
                    }
                }).then(function () {
                    var selectElement = $('#drpDepots');

                    var selectedValues = selectElement.val();

                    // Clear the current selection
                    selectElement.val(null).trigger('change');

                    // Refresh the Select2 dropdown and re-select previously selected options
                    populateDepots(countryId, function () {
                        $('#drpDepots').select2({
                            placeholder: 'Select an option',
                            allowClear: true,
                        });
                        if (selectedValues) {
                            selectElement.val(selectedValues).trigger('change');
                        }
                    });
                });
            }
        },
        error: function (data) {
            swal({
                title: 'Error!',
                text: 'An error occurred while adding the depot. Please try again.',
                icon: 'error',
                button: {
                    text: "Continue",
                    value: true,
                    visible: true,
                    className: "btn btn-danger"
                }
            });
        }
    });
}

function populateProduct(productID) {
    $.ajax({
        type: "GET",
        url: "/Client/GetProducts",
        dataType: "json",
        success: function (data) {

            var index = -1;
            for (var x = 0; x < data.length; x++) {
                if (data[x].productId == productID) {
                    index = x;
                    break;
                }
            }

            for (var x = 0; x < data.length; x++) {
                if (x != index) {
                    $("#drpProducts").append('<option  id="' + data[x].productId + '" value="' + data[x].productId + '">' + data[x].productName + '</option>');
                }
                else if (index = ! -1) {
                    $("#drpProducts").append('<option  id="' + data[x].productId + '" value="' + data[x].productId + '">' + data[x].productName + '</option>');
                }
            }
        },
        error: function (data) {
        }
    })
}
function populateProducts() {
    $.ajax({
        type: "GET",
        url: "/Client/GetProducts",
        dataType: "json",
        success: function (data) {
            $("#drpProducts").empty();
            for (var x = 0; x < data.length; x++) {
                $("#drpProducts").append('<option  id="product' + data[x].productId + '" value="' + data[x].productId + '">' + data[x].productName + '</option>');
            }
        },
        error: function (data) {
        }
    })
}


function getBillingDate() {
    const values = [];
    const myArray = Array.from({ length: 31 }, (_, i) => i + 1);
    const selectedIndex = 0;
    const selectedValue = myArray[selectedIndex];
    $("#drpBillingDate").append(`<option id="${selectedValue}" value="${selectedValue}">${selectedValue}</option>`);
    values.push(selectedValue);
    for (let i = 0; i < myArray.length; i++) {
        if (i === selectedIndex) {
            continue;
        }
        const value = myArray[i];
        $("#drpBillingDate").append(`<option id="${value}" value="${value}">${value}</option>`);
        values.push(value);
    }
    return values;
}

function getBillingCurrency() {
    const values = [];
    const myArray = ['ZAR', 'NGN', 'EUR', 'Z$', 'SZL', 'BWP', 'TZS'];
    const selectedIndex = 0;
    const selectedValue = myArray[selectedIndex];
    $("#drpBillingCurrency").append(`<option id="${selectedValue}" value="${selectedValue}">${selectedValue}</option>`);
    values.push(selectedValue);
    for (let i = 0; i < myArray.length; i++) {
        if (i === selectedIndex) {
            continue;
        }
        const value = myArray[i];
        $("#drpBillingCurrency").append(`<option id="${value}" value="${value}">${value}</option>`);
        values.push(value);
    }
    return values;
}



function newGetSla() {

   // const myArray = [true, false];
    const selectedIndex = -1;
    //data.sla = myArray[selectedIndex];      
    const $drpSLA = $("#drpSLA");
    $drpSLA.append(`<option value="${data.sla}">${data.sla}</option>`);
    for (let i = 0; i < myArray.length; i++) {
        if (i === selectedIndex) continue;
    }
    if (selectedIndex != -1) {
        $drpSLA.append(`<option value="${myArray[i]}">${myArray[i]}</option>`);
    };
    for (var x = 0; x < myArray.length; x++) {
        if (x != selectedIndex) {
            $drpSLA.append(`<option value="${myArray[i]}">${myArray[i]}</option>`);
        }
    }

}


function getSla() {
    const myArray2 = [];
    const myArray = [true, false];
    const selectedIndex = 0;
    const selectedValue = myArray[selectedIndex];
    const $drpSLA = $("#drpSLA");
    // add selected value as first option
    $drpSLA.append(`<option value="${selectedValue}">${selectedValue}</option>`);

    // add remaining options
    for (let i = 0; i < myArray.length; i++) {
        if (i === selectedIndex) continue; // skip selected index
        $drpSLA.append(`<option value="${myArray[i]}">${myArray[i]}</option>`);
        myArray2.push(myArray);
    }
    return myArray2;
}

function getAccManager(userID) {
    $.ajax({
        type: "GET",
        url: "/User/GetAccountManager",
        dataType: "json",
        success: function (data) {

            var index = -1;
            //var index = indexOf(data => data.roleID == roleID);
            for (var x = 0; x < data.length; x++) {
                if (data[x].userId == userID) {
                    index = x;
                    break;
                }
            }
            if (index != -1) {
                $("#drpAccountManager").append('<option  id="' + data[index].userId + '" value="' + data[index].userId + '">' + data[index].accountManger + '</option>');
            };
            /*$("#drpRoles").append('<option  value="-1" > -- Select A Role -- </option>');*/
            for (var x = 0; x < data.length; x++) {
                if (x != index) {
                    $("#drpAccountManager").append('<option  id="' + data[x].userId + '" value="' + data[x].userId + '">' + data[x].accountManger + '</option>');
                }
            }
        },
        error: function (data) {
        }
    })
}

function getAccountManager() {
    $.ajax({
        type: "GET",
        url: "/Client/GetAccountManager",
        dataType: "json",
        success: function (data) {
            $("#drpAccountManager").empty();
            /* $("#drpAccountManager").append('<option  value="-1" > -- Select An Account Manager -- </option>');*/
            for (var x = 0; x < data.length; x++) {
                $("#drpAccountManager").append('<option  id="' + data[x].userId + '" value="' + data[x].userId + '">' + data[x].name + '</br> ' + data[x].surname + '</option>');

            }
        },
        error: function (data) {
        }
    })
}

function getCountry(countryID) {
    $.ajax({
        type: "GET",
        url: "/Client/GetCountries",
        dataType: "json",
        success: function (data) {

            var index = -1;
            for (var x = 0; x < data.length; x++) {
                if (data[x].countryId == countryID) {
                    index = x;
                    break;
                }
            }
            if (index = ! -1) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryName + '">' + data[x].countryName + '</option>');
            }
            for (var x = 0; x < data.length; x++) {
                if (x != index) {
                    $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
                }
            }
        },
        error: function (data) {
        }
    })
}
function getCountries() {

    $.ajax({
        type: "GET",
        url: "/Client/GetCountries",
        dataType: "json",
        success: function (data) {

            $("#drpCountries").empty();
            $("#drpCountries").append('<option  value="-1" > -- Select A Country -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
            }
        },
        error: function (data) {
        }
    })
}

function getDepoProducts(depotID) {
    $.ajax({
        type: "GET",
        url: "/User/GetProducts",
        dataType: "json",
        success: function (data) {

            var index = -1;           
            for (var x = 0; x < data.length; x++) {
                if (data[x].depotId == depotID) {
                    index = x;
                    break;
                }
            }
            if (index != -1) {
                $("#drpDepots").append('<option  id="' + data[index].productId + '" value="' + data[index].productId + '">' + data[index].productName + '</option>');
            };
            for (var x = 0; x < data.length; x++) {
                if (x != index) {
                    $("#drpDepots").append('<option  id="' + data[x].productId + '" value="' + data[x].productId + '">' + data[x].productName + '</option>');
                }
            }
        },
        error: function (data) {
        }
    })
}


function validate() {
    if ($("#txtName").val() === '') {
        $("#txtName").css('border', '2px red solid');
        return false;
    } else {
        $("#txtName").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpCountries").val() === '') {
        $("#drpCountries").css('border', '2px red solid');
        return false;
    } else {
        $("#drpCountries").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpDepots").val() === '') {
        $("#drpDepots").css('border', '2px red solid');
        return false;
    } else {
        $("#drpDepots").css('border', '1px #c9c8c8 solid');
    }   
    if ($("#drpProducts").val() === '') {
        $("#drpProducts").css('border', '2px red solid');
        return false;
    } else {
        $("#drpProducts").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpAccountManager").val() === '') {
        $("#drpAccountManager").css('border', '2px red solid');
        return false;
    } else {
        $("#drpAccountManager").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpSupportRate").val() === '') {
        $("#drpSupportRate").css('border', '2px red solid');
        return false;
    } else {
        $("#drpSupportRate").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpBillingCurrency").val() === '') {
        $("#drpBillingCurrency").css('border', '2px red solid');
        return false;
    } else {
        $("#drpBillingCurrency").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpBillingDate").val() === '') {
        $("#drpBillingDate").css('border', '2px red solid');
        return false;
    } else {
        $("#drpBillingDate").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpSLA").val() === '') {
        $("#drpSLA").css('border', '2px red solid');
        return false;
    } else {
        $("#drpSLA").css('border', '1px #c9c8c8 solid');
    }

    if ($("#drpSLA :selected").val() === 'false') {
        if ($("#txtSLADetails").val() === '') {
           // $("#txtSLADetails").css('border', '2px red solid');
            return true;
        } else {
            $("#txtSLADetails").css('border', '1px transparent solid');
        }
    }

    return true;
}

function sLAChanged() {

   if ($("#drpSLA :selected").val() === 'false') {
       $("#txtSLADetails").attr('disabled', 'disabled');
       $("#txtSLADetails").val('') 
        return false;
    } else {
        $("#txtSLADetails").removeAttr('disabled');
    }

}

$('#drpSLA').on('change', function () {

    if ($("#drpSLA :selected").val() === 'false') {
        $("#txtSLADetails").attr('disabled', 'disabled');
        $("#txtSLADetails").val('') 
        return false;
    } else {
        $("#txtSLADetails").removeAttr('disabled');
    }
})

