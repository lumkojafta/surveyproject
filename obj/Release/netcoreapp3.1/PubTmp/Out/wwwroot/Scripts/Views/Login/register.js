﻿$(document).ready(function () {

    $('#userName').val('');
    $('#email').val('');
    $('#passwordReg').val('');

    $('#passwordReg').keyup(function () {
        $('#strengthMessage').html(checkStrength($('#passwordReg').val()))
    })
    function checkStrength(password) {
        var strength = 0
        if (password.length < 6) {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Short')
            return 'Too short'
        }
        if (password.length > 7) strength += 1
        // If password contains both lower and uppercase characters, increase strength value.  
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
        // If it has numbers and characters, increase strength value.  
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
        // If it has one special character, increase strength value.  
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
        // If it has two special characters, increase strength value.  
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
        // Calculated strength value, we can return messages  
        // If value is less than 2  
        if (strength < 2) {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Weak')
            return 'Weak'
        } else if (strength == 2) {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Good')
            return 'Good'
        } else {
            $('#strengthMessage').removeClass()
            $('#strengthMessage').addClass('Strong')
            return 'Strong'
        }
    }
});

function register() {

    var name = $('#userName').val();
    var emailAddress = $('#email').val();
    var pwd = $('#passwordReg').val();
    var url = 'https://' + location.host;
    var password = btoa(pwd);

    var validate = validateRegister();
    var isValidEmail = validateEmail(emailAddress);
    if (isValidEmail) {
        $('#email').parent().css('border', 'transparent 0px solid');
        if (validate) {
            if ($('#strengthMessage').html() === 'Strong' || $('#strengthMessage').html() === 'Strong') {
                $.ajax({
                    method: "POST",
                    url: '/Login/RegisterUser',
                    //contentType: "application/json; charset=utf-8",
                    data: {
                        username: name,
                        email: emailAddress,
                        password: password,
                        url: url, 
                    },
                    cache: false
                })
                    .done(function (data) {
                        if (data === "User Already Exists") {
                            swal({
                                title: 'Oops...',
                                text: 'User Already Exists.',
                                icon: 'error',
                                button: {
                                    text: "Continue",
                                    value: true,
                                    visible: true,
                                    className: "btn btn-primary"
                                }
                            })
                        } else {
                            swal({
                                title: 'Congratulations!',
                                text: 'You have successfully registered a user, please check your email for verification.',
                                icon: 'success',
                                button: {
                                    text: "Continue",
                                    value: true,
                                    visible: true,
                                    className: "btn btn-primary"
                                }
                            })
                        }
                    })
                    .fail(function (ajaxRequest, status, error) {
                        console.warn('ajaxRequest: ', ajaxRequest);
                        console.warn('status: ', status);
                        console.warn('error: ', error);
                    });
            } else {
                $('#passwordReg').parent().css('border', 'red 2px solid');
            }
        }
    } else {
        $('#email').parent().css('border', 'red 2px solid');
    }
}

function validateRegister() {
    if ($('#userName').val() === '') {
        $('#userName').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#userName').parent().css('border', 'transparent 0px solid');
    }
    if ($('#email').val() === '') {
        $('#email').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#email').parent().css('border', 'transparent 0px solid');

    }
    if ($('#passwordReg').val() === '') {
        $('#passwordReg').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#passwordReg').parent().css('border', 'transparent 0px solid');

    }
    return true;
}

function showPassword() {
    var x = document.getElementById("passwordReg");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return email.match(re)
}