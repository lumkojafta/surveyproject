﻿var userReportTable;

$(document).ready(function () {
    getUsers();
    initDataTable();
});

function getUsers() {

    $.ajax({
        type: "GET",
        url: "/SupportLogging/GetUsers",
        dataType: "json",
        success: function (data) {
            $("#drpUserName").empty();
            $("#drpUserName").append('<option  value="-1" > -- Select A User -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpUserName").append('<option  id="' + data[x].userId + '" value="' + data[x].userId + '">' + data[x].userName + '</option>');
            }
        },
        error: function (data) {



        }

    })
}



function loadUserReoprt() {

    userReportTable.clear().draw();
    userReportTable.ajax.reload();
}

function initDataTable() {


    userReportTable = $('#userReportTable').DataTable({
        //"bPaginate": !SiteDataTableSettings.IsNoTablePagingPath,
        pagingType: 'full_numbers',
        columns: [
            {
                'title': 'Date',
                'data': 'date',
                //render: function (data, type, row) {
                //    var dt = new Date(row.Date);
                //    return type === 'sort' ? dt : moment(dt).format('DD/MM/YYYY');
                //}
            },
            {
                'title': 'Total Time HH:MM',
                'data': 'timeSpentOnSupport'
            }, {
                'title': 'Client Name',
                'data': 'clientName'
            }, {
                'title': 'Country',
                'data': 'country'
            }, {
                'title': 'Depot Code',
                'data': 'depotCode'
            }, {
                'title': 'Product Name',
                'data': 'productName'
            }, {
                'title': 'Issue Description',
                'data': 'issueDescription'
            },
            {
                'title': 'Resolution Desc',
                'data': 'resolutionDesc'
            }, {
                'title': 'Valid For Billing',
                render: function (data, type, row) {
                    return '<label class="badge badge-' + (row.validForBilling ? 'success' : 'info') + '">' + row.validForBilling + '</label>';
                }
            },
            {
                'title': 'Reason',
                'data': 'reasonForNotBilling'
            },
            {
                'title': 'Assited By',
                'data': 'assitedBy'
            }          
        ],
        columnDefs: [
            { type: 'date', targets: 0 },

        ],
        ajax: {
            method: 'GET',
            url: '/Reports/GetUserReport',
            data: function (d) {
                d.userId = $('#drpUserName :selected').val(),
                    d.month = $('#drpMonth :selected').val()
                   
            }
        }
    });
   

    $('#userReportTable').each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });
};