﻿$(document).ready(function () {
    
});

function login() {
    var email = $('#name').val();
    var password = $('#password').val();

    var validate = validateLogin();
    if (validate) {
        $.ajax({
            method: "GET",
            url: '/Login/Login',
            contentType: "application/json; charset=utf-8",
            data: {
                email: email,
                password: password,
            },
            cache: false
        })
            .done(function (data) {
                if (data === 'Invalid Username Or Password') {
                    $('#divPasswordError').css('display', 'block');
                    return false;
                }
                else if (data === 'Please Check Your Email To Activate The User') {
                    swal({
                        title: 'Oops...',
                        text: data,
                        icon: 'error',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    })
                }
                else {
                    var url = "";
                    if (window.location.href.includes("login/index")) {
                        url = window.location.href.replace("login/index", "/SupportLogging/LandingPage");
                    } else {
                        url = "/SupportLogging/LandingPage";
                    }
                    window.location.href = url;
                }
            })
            .fail(function (ajaxRequest, status, error) {
                console.warn('ajaxRequest: ', ajaxRequest);
                console.warn('status: ', status);
                console.warn('error: ', error);
            });
    }
}



function validateLogin() {
    if ($('#name').val() === '') {
        $('#name').parent().css('border', 'red 2px solid');
        return false;

    } else {
        $('#name').parent().css('border', 'transparent 0px solid');
        
    }


    if ($('#password').val() === '') {
        $('#password').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#password').parent().css('border', 'transparent 0px solid');

    }
    return true;
}

function showPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}