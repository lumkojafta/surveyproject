﻿var addUserTable;
$(document).ready(function () {
    Init();
    getUsersRoles();
});
function Init() {
    //create table
    addUserTable = $('#addUserTable').DataTable({
        "ajax": {
            "method": 'GET',
            "url": '/User/GetUsers',
            "dataSrc": '',
        },
        pagingType: 'full_numbers',
        "columns": [
            {
                'title': 'Name',
                "data": "name"
            },
            {
                'title': 'Surname',
                "data": "surname"
            },
            {
                'title': 'UserName',
                "data": "userName"
            },           
            {
                'title': 'Role',
                "data": "roleName"
            },
            {
                'title': 'Password',
                "data": "password"
            },
            {
                'title': 'Edit',
                render: function (data, type, row) {
                    
                    return '<button id="' + row.userId + '" onclick="updateUser(' + row.userId + ')"; return false;" class="btn btn-outline-primary">View</button>'
                    +'&nbsp &nbsp;'
                    + '<button id="' + row.userId + '" onclick="deleteUser(' + row.userId + ')"; return false;" class="btn btn-outline-primary">Delete</button>';
                }
            },

        ],
        //columnDefs: [{
        //    "defaultContent": "-",
        //    "targets": "_all"
        //}]
    });
    $('#addUserTable').each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });
};
function AddNewUser() {
    $('#divAddUser').show();
    $('#divTableContainer').hide();
    $("#txtName").val("");
    $("#txtUserName").val("");
    $("#txtEmailAddress").val("");
    $("#txtPassword").val("");
    $("#txtSurname").val("");
}
function cancel() {
    $('#divAddUser').hide();
    $('#divTableContainer').show();
}
function resetForm($form1) {
    $form.find('input:txtName, input:txtSurname').val('');
}
function saveUser() {
    // $('#divAddUser').hide();
    //$('#divTableContainer').show();

    //var id = parseInt(userdata);
    //$('#userId').val(id);
    //var data = { 'userId': id }
    isModalValid = validate();
    if (isModalValid) {
        var id = $('#userId').val();
        var name = $("#txtName").val();
        var username = $("#txtUserName").val();
        var surname = $("#txtSurname").val();
        /*var roleName = $("#txtSurname").val();*/
        var username = $("#txtUserName").val();
        var emailAddress = $("#txtEmailAddress").val();
        var role = $("#roleId :selected").val();
        var rolename = $("#roleId :selected").text();
        var password = $("#txtPassword").val();
        var model = {

            UserId: id, Name: name, UserName: username, Surname: surname, EmailAddress: emailAddress, RoleName: rolename, Password: password,
            UserName: username, RoleId: role
        };
        console.log("model", model);
        $.ajax({
            type: "POST",
            url: "/User/SaveUser",
            data: model,
            dataType: "json",
            success: function (data) {
                
                swal({
                    title: 'Success!',
                    text: 'You have successfully added a new user',
                    icon: 'success',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"
                    }
                }).then(function () {
                    window.location.reload();
                });
            },
            error: function (data) {
                if (data.isSuccess == false) {
                    alert(data.errorMsg);
                }
            }
        })
    }
}
function deleteUser(userId) {
    var id = parseInt(userId);
    $('#userId').val(id);
    var userId = { 'userId': id }

    $.ajax({
        type: "DELETE",
        url: "/User/DeleteUser",
        dataType: "json",
        data: userId,
        success: function (data) {   
            swal({
                title: 'Success!',
                text: 'Record deleted Succesfully',
                icon: 'success',
                button: {
                    text: "Continue",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            }).then(function () {
                window.location.reload();
            });
        },
        error: function (data) {
            if (data.isSuccess == false) {
                alert(data.errorMsg);
            }
        }
    })

}
function updateUser(userdata) {
    var id = parseInt(userdata);
    $('#userId').val(id);
    var data = { 'userId': id }    
    //$("#userId").val(id);
    //var userId = { 'userId': id }
    $('#divAddUser').show();
    $('#divTableContainer').hide();
    $.ajax({
        method: "GET",
        url: '/User/EditUser',
        dataType: "json",
        data: data,
        cache: false
    })
        .done(function (data) {
            // $('#divEditSupportCase').css('display', 'block');          
           
            $("#txtName").val(data.name);
            $("#txtSurname").val(data.surname);
            $("#txtUserName").val(data.userName);
            $("#txtEmailAddress").val(data.emailAddress);
            $("#roleId").empty();
            getRoles(data.roleId);
            $("#roleId").append('<option  id="' + data.roleName + '" value="' + data.roleName + '">' + data.roleName + '</option>');
            $("#roleId :selected").text(data.roleName);
            $("#txtPassword").val(data.password);
        })
        .fail(function (ajaxRequest, status, error) {
        })
        .always(function () {
        });
};
function getRoles(roleID) {
    $.ajax({
        type: "GET",
        url: "/User/GetRoles",
        dataType: "json",
        success: function (data) {          

            var index = -1;
            //var index = indexOf(data => data.roleID == roleID);
            for (var x = 0; x < data.length; x++) {
                if (data[x].roleid == roleID) {
                    index = x;
                    break;
                }
            }
            if (index != -1) {
                $("#roleId").append('<option  id="' + data[index].roleId + '" value="' + data[index].roleId + '">' + data[index].roleName + '</option>');
            };
            /*$("#drpRoles").append('<option  value="-1" > -- Select A Role -- </option>');*/
            for (var x = 0; x < data.length; x++) {
                if (x != index) {
                    $("#roleId").append('<option  id="' + data[x].roleId + '" value="' + data[x].roleId + '">' + data[x].roleName + '</option>');
                }
            }
        },
        error: function (data) {
        }
    })
}
function validate() {
    if ($("#txtName").val() === '') {
        $("#txtName").css('border', '2px red solid');
        return false;
    } else {
        $("#txtName").css('border', '1px #c9c8c8 solid');
    }
    if ($("#txtSurname").val() === '') {
        $("#txtSurname").css('border', '2px red solid');
        return false;
    } else {
        $("#txtSurname").css('border', '1px #c9c8c8 solid');
    }
    if ($("#drpRole").val() === '') {
        $("#drpRole").css('border', '2px red solid');
        return false;
    } else {
        $("#drpRole").css('border', '1px #c9c8c8 solid');
    }
    if ($("#txtPassword").val() === '') {
        $("#txtPassword").css('border', '2px red solid');
        return false;
    } else {
        $("#txtPassword").css('border', '1px #c9c8c8 solid');
    }
    return true;
}
function getUsersRoles() {

    $.ajax({
        type: "GET",
        url: "/User/GetRoles",
        dataType: "json",
        success: function (data) {

            $("#roleId").empty();
            $("#roleId").append('<option  value="-1" > -- Select A Role -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#roleId").append('<option  id="' + data[x].roleId + '" value="' + data[x].roleId + '">' + data[x].roleName + '</option>');
            }
        },
        error: function (data) {
        }
    })
}
