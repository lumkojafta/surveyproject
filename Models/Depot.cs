﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    public partial class Depot
    {
        public Depot()
        {
            DepotProducts = new HashSet<DepotProduct>();
        }

        [Key]
        [Column("DepotID")]
        public int DepotId { get; set; }
        [Required]
        [StringLength(50)]
        public string DepotCode { get; set; }

        [InverseProperty(nameof(DepotProduct.Depot))]
        public virtual ICollection<DepotProduct> DepotProducts { get; set; }
    }
}
