﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    public partial class User
    {
        [Key]
        public int UserId { get; set; }
        [StringLength(150)]
        public string EmailAddress { get; set; }
        [Column("RoleID")]
        public int RoleId { get; set; }
        [Required]
        [StringLength(100)]
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        [StringLength(255)]
        public string UserName { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty("Users")]
        public virtual Role Role { get; set; }
    }
}
