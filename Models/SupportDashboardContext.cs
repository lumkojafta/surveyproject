﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace SupportDash.Models
{
    public partial class SupportDashboardContext : DbContext
    {
        public SupportDashboardContext()
        {
        }

        public SupportDashboardContext(DbContextOptions<SupportDashboardContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<ClientProduct> ClientProducts { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CountryClient> CountryClients { get; set; }
        public virtual DbSet<Depot> Depots { get; set; }
        public virtual DbSet<DepotCountry> DepotCountries { get; set; }
        public virtual DbSet<DepotProduct> DepotProducts { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SupportCase> SupportCases { get; set; }
        public virtual DbSet<User> Users { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Server=DESKTOP-5L19LTM\\SQLEXPRESS;Database=SupportDashboard;MultipleActiveResultSets=true;Trusted_Connection=True;");

            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.AccountManger).IsUnicode(false);

                entity.Property(e => e.BillingCurrency).IsUnicode(false);

                entity.Property(e => e.BillingDate).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Sladetals).IsUnicode(false);

                entity.Property(e => e.SupportRate).IsUnicode(false);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.CountryName).IsUnicode(false);
            });

            modelBuilder.Entity<Depot>(entity =>
            {
                entity.Property(e => e.DepotCode).IsUnicode(false);
            });

            modelBuilder.Entity<DepotProduct>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Depot)
                    .WithMany(p => p.DepotProducts)
                    .HasForeignKey(d => d.DepotId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DepotProducts_Depots");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.DepotProducts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DepotProducts_Products");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.ProductName).IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.RoleName).IsUnicode(false);
            });

            modelBuilder.Entity<SupportCase>(entity =>
            {
                entity.Property(e => e.AssitedBy).IsUnicode(false);

                entity.Property(e => e.ClientName).IsUnicode(false);

                entity.Property(e => e.Country).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.DepotCode).IsUnicode(false);

                entity.Property(e => e.EndTime).IsUnicode(false);

                entity.Property(e => e.IssueDescription).IsUnicode(false);

                entity.Property(e => e.ProductName).IsUnicode(false);

                entity.Property(e => e.ReasonForNotBilling).IsUnicode(false);

                entity.Property(e => e.RequestorName).IsUnicode(false);

                entity.Property(e => e.ResolutionDesc).IsUnicode(false);

                entity.Property(e => e.ResonForEditing).IsUnicode(false);

                entity.Property(e => e.StartTime).IsUnicode(false);

                entity.Property(e => e.SupportRef).IsUnicode(false);

                entity.Property(e => e.TimeSpentOnSupport).IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.EmailAddress).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Surname).IsUnicode(false);

                entity.Property(e => e.UserName).IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_Roles");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
