﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace SupportDash.Models
{
    public class UserView
    {
        public int UserId { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }
        public List<SelectList> Role { get; set; }
    }
}
