﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    public partial class SupportCase
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        [Required]
        [StringLength(100)]
        public string Country { get; set; }
        [Required]
        [StringLength(50)]
        public string DepotCode { get; set; }
        [Required]
        [StringLength(100)]
        public string RequestorName { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Required]
        [StringLength(50)]
        public string StartTime { get; set; }
        [Required]
        public string IssueDescription { get; set; }
        public bool ValidForBilling { get; set; }
        public string ReasonForNotBilling { get; set; }
        [Required]
        [StringLength(100)]
        public string AssitedBy { get; set; }
        [Required]
        [StringLength(50)]
        public string EndTime { get; set; }
        [Required]
        [StringLength(50)]
        public string TimeSpentOnSupport { get; set; }
        public string ResolutionDesc { get; set; }
        [StringLength(50)]
        public string SupportRef { get; set; }
        [StringLength(50)]
        public string ResonForEditing { get; set; }
        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }
    }
}
