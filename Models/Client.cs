﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    public partial class Client
    {
        [Key]
        [Column("ClientID")]
        public int ClientId { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength(250)]
        public string AccountManger { get; set; }
        [StringLength(250)]
        public string SupportRate { get; set; }
        [StringLength(250)]
        public string BillingCurrency { get; set; }
        [StringLength(250)]
        public string BillingDate { get; set; }
        [Column("SLA")]
        public bool? Sla { get; set; }
        [Column("SLADetals")]
        public string Sladetals { get; set; }
    }
}
