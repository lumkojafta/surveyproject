﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SupportDash.Models
{
    public class ClientViewModel
    {
        
        public int ClientId { get; set; }
     
        public string Name { get; set; }
       
        public string Country { get; set; }
    
        public List<string> DepotCode { get; set; }
        public List<string> AllDepotCodes { get; set; }
        public List<int> DepotIDs { get; set; }
        
        public List<string> ProductName { get; set; }

        public List<string> AllProducts { get; set; }
        public List<int> ProductIDs { get; set; }

        public string AccountManger { get; set; }
       
        public string SupportRate { get; set; }
       
        public string BillingCurrency { get; set; }
        
        public string BillingDate { get; set; }
       
        public bool? Sla { get; set; }       
        public string Sladetails { get; set; }

        public int CountryId { get; set; }

        public int ProductId { get; set; }

        //[InverseProperty(nameof(ClientProduct.Client))]
        public virtual ICollection<ClientProduct> ClientProducts { get; set; }
        [InverseProperty("Client")]
        public virtual ICollection<Country> Countries { get; set; }
        public List<Product> Products { get; set; }
        public List<Depot> Depots { get; set; }
    }
}
