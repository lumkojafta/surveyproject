﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    public partial class Product
    {
        public Product()
        {
            DepotProducts = new HashSet<DepotProduct>();
        }

        [Key]
        [Column("ProductID")]
        public int ProductId { get; set; }
        [Required]
        public string ProductName { get; set; }

        [InverseProperty(nameof(DepotProduct.Product))]
        public virtual ICollection<DepotProduct> DepotProducts { get; set; }
    }
}
