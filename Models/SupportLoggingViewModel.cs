﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace SupportDash.Models
{
    public class SupportLoggingViewModel
    {
        public int Id { get; set; }

        public string ClientName { get; set; }

        public string Country { get; set; }

        public string DepotCode { get; set; }

        public string RequestorName { get; set; }

        public string ProductName { get; set; }

        public string Date { get; set; }

        public string StartTime { get; set; }

        public string IssueDescription { get; set; }
        public bool ValidForBilling { get; set; }
        public string ReasonForNotBilling { get; set; }

        public string AssitedBy { get; set; }

        public string EndTime { get; set; }

        public string TimeSpentOnSupport { get; set; }
        public string ResolutionDesc { get; set; }

        public string SupportRef { get; set; }

        public string ResonForEditing { get; set; }

        public string CreatedBy { get; set; }

    }
}
