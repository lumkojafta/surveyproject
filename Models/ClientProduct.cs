﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    [Table("ClientProduct")]
    public partial class ClientProduct
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("ClientID")]
        public int ClientId { get; set; }
        [Column("ProductID")]
        public int ProductId { get; set; }
    }
}
