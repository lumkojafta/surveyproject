﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    [Table("DepotCountry")]
    public partial class DepotCountry
    {
        [Key]
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int DepotId { get; set; }
        public int ClientId { get; set; }
    }
}
