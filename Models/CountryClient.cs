﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    [Table("CountryClient")]
    public partial class CountryClient
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int ClientId { get; set; }
    }
}
