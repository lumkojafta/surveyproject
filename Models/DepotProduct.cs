﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace SupportDash.Models
{
    public partial class DepotProduct
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int DepotId { get; set; }

        [ForeignKey(nameof(DepotId))]
        [InverseProperty("DepotProducts")]
        public virtual Depot Depot { get; set; }
        [ForeignKey(nameof(ProductId))]
        [InverseProperty("DepotProducts")]
        public virtual Product Product { get; set; }
    }
}
