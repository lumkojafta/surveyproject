(function($) {
  'use strict';
    if ($("#startTime").length) {
        $('#startTime').datetimepicker({
      format: 'LT'
    });
    }
    if ($("#endTime").length) {
        $('#endTime').datetimepicker({
            format: 'LT'
        });
    }
    if ($("#timeDiff").length) {
        $('#timeDiff').datetimepicker({
            format: 'LT'
        });
    }
  if ($(".color-picker").length) {
    $('.color-picker').asColorPicker();
  }
  if ($("#datepicker-popup").length) {
    $('#datepicker-popup').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if ($("#inline-datepicker").length) {
    $('#inline-datepicker').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if ($(".datepicker-autoclose").length) {
    $('.datepicker-autoclose').datepicker({
      autoclose: true
    });
  }
  if($('.input-daterange').length) {
    $('.input-daterange input').each(function() {
      $(this).datepicker('clearDates');
    });
    $('.input-daterange').datepicker({});
  }
})(jQuery);