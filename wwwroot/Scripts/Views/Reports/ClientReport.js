﻿var clientReportTable;

$(document).ready(function () {
    getClients();
    getUsers();
    initDataTable();
});

function getClients() {

    $.ajax({
        type: "GET",
        url: "/SupportLogging/GetClients",
        dataType: "json",
        success: function (data) {

            $("#drpClients").empty();
            $("#drpClients").append('<option  value="-1" > -- Select A Client -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpClients").append('<option  id="' + data[x].clientId + '" value="' + data[x].clientId + '">' + data[x].name + '</option>');
            }
        },
        error: function (data) {



        }

    })
}

function getUsers() {

    $.ajax({
        type: "GET",
        url: "/SupportLogging/GetUsers",
        dataType: "json",
        success: function (data) {

            $("#drpAssitedBy").empty();
            $("#drpAssitedBy").append('<option  value="-1" > -- Select A Client -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpAssitedBy").append('<option  id="' + data[x].userId + '" value="' + data[x].userId + '">' + data[x].userName + '</option>');
            }
        },
        error: function (data) {

        }

    })
}

function populateCounties() {
    var clientId = $("#drpClients :selected").val();

    var model = {
        ClientId: clientId
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetCountries",
        data: model,
        dataType: "json",
        success: function (data) {


            $("#drpCountries").empty();
            $("#drpCountries").append('<option  value="-1" > -- Select A Country -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
            }

        },
        error: function (data) {

        }

    })
}

function populateDepots() {
    var countryId = $("#drpCountries :selected").val();

    var model = {
        CountryId: countryId
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetDepots",
        data: model,
        dataType: "json",
        success: function (data) {

            $("#drpDepots").empty();
            $("#drpDepots").append('<option  value="-1" > -- Select A Depot -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpDepots").append('<option  id="' + data[x].depotId + '" value="' + data[x].depotId + '">' + data[x].depotCode + '</option>');
            }

        },
        error: function (data) {

        }

    })
}


function loadSupportCase() {

    clientReportTable.clear().draw();
    clientReportTable.ajax.reload();
}

function initDataTable() {


    clientReportTable = $('#clientReportTable').DataTable({
        //"bPaginate": !SiteDataTableSettings.IsNoTablePagingPath,
        pagingType: 'full_numbers',
        columns: [
            {
                'title': 'Date',
                'data': 'date',
                //render: function (data, type, row) {
                //    var dt = new Date(row.Date);
                //    return type === 'sort' ? dt : moment(dt).format('DD/MM/YYYY');
                //}
            },
            {
                'title': 'Total Time',
                'data': 'timeSpentOnSupport'
            }, {
                'title': 'Client Name',
                'data': 'clientName'
            }, {
                'title': 'Country',
                'data': 'country'
            }, {
                'title': 'Depot Code',
                'data': 'depotCode'
            }, {
                'title': 'Product Name',
                'data': 'productName'
            }, {
                'title': 'Issue Description',
                'data': 'issueDescription'
            },
            {
                'title': 'Resolution Desc',
                'data': 'resolutionDesc'
            }, {
                'title': 'Valid For Billing',
                render: function (data, type, row) {
                    return '<label class="badge badge-' + (row.validForBilling ? 'success' : 'info') + '">' + row.validForBilling + '</label>';
                }
            },
            {
                'title': 'Reason',
                'data': 'reasonForNotBilling'
            },
            {
                'title': 'Assited By',
                'data': 'assitedBy'
            }          
        ],
        columnDefs: [
            { type: 'date', targets: 0 },

        ],
        ajax: {
            method: 'GET',
            url: '/Reports/GetSuportCases',
            data: function (d) {
                d.toDate = $('#txtEndDate').val(),
                    d.fromDate = $('#txtStartDate').val(),
                    d.client = $("#drpClients :selected").text(),
                    d.country = $("#drpCountries :selected").text(),
                    d.depot = $("#drpDepots :selected").text(),
                    d.reportType = $("#drpReportType :selected").text()
            }
        }
    });

    $('#clientReportTable').each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });
};