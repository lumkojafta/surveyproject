﻿$(document).ready(function () {
    populateAllCounties();
    getUsers();
    $('#drpDepots').select2({
        placeholder: 'Select an option',
        allowClear: true,
    });

});

function saveSupportCase() {

    isModalValid = validate();
    if (isModalValid) {

        var client = $("#drpClients :selected").text();
        var country = $("#drpCountries :selected").text();
        var depot = $("#drpDepots").val();
        depot = depot.join(',');
        var reuestorName = $("#txtRequestorName").val();
        var products = $("#drpProducts :selected").text();
        var date = $("#txtDate").val();
        var startTime = $("#txtStartTime").val();
        var description = $("#txtDescription").val();
        var resolutionDesc = $("#txtResolutionDesc").val();
        var validForBilling = $("#drpValidForBilling :selected").val();
        var reason = $("#txtReason").val();
        var assitedBy = $("#drpAssitedBy :selected").text();
        var endDate = $("#txtEndDate").val();
        var timeSpent = $("#txtTimeSpent").val();

        var model = {
            ClientName: client, Country: country, DepotCode: depot, RequestorName: reuestorName,
            ProductName: products, Date: date, StartTime: startTime, IssueDescription: description,
            ResolutionDesc: resolutionDesc, ValidForBilling: validForBilling, ReasonForNotBilling: reason, AssitedBy: assitedBy,
            EndTime: endDate, TimeSpentOnSupport: timeSpent,
        };

        $.ajax({
            type: "POST",
            url: "/SupportLogging/EditSupportCase",
            data: model,
            dataType: "json",
            success: function (data) {              
                swal({
                    title: 'Success!',
                    text: 'You have successfully added a new support case',
                    icon: 'success',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"
                    }
                }).then(function () {
                    window.location.reload();
                });

            },
            error: function (data) {

                if (data.isSuccess == false) {
                    alert(data.errorMsg);
                }

            }

        })
    }
}

function getClients() {

    $.ajax({
        type: "GET",
        url: "/SupportLogging/GetClients",
        dataType: "json",
        success: function (data) {
            $("#drpClients").empty();
            $("#drpClients").append('<option  value="-1" > -- Select A Client -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpClients").append('<option  id="' + data[x].clientId + '" value="' + data[x].clientId + '">' + data[x].name + '</option>');
            }
        },
        error: function (data) {

        }

    })
}
function getClientsByCountryId() {

    var countryId = $("#drpCountries :selected").val();

    var model = {
        countryId: countryId
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetClientsByCountryId",
        data: model,
        dataType: "json",
        success: function (data) {
            $("#drpClients").empty();
            $("#drpClients").append('<option  value="-1" > -- Select A Client -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpClients").append('<option  id="' + data[x].clientId + '" value="' + data[x].clientId + '">' + data[x].name + '</option>');
            }
        },
        error: function (data) {

        }

    })
}



    function getUsers() {

        $.ajax({
            type: "GET",
            url: "/SupportLogging/GetUsers",
            dataType: "json",
            success: function (data) {
                $("#drpAssitedBy").empty();
                $("#drpAssitedBy").append('<option  value="-1" > -- Select A User -- </option>');
                for (var x = 0; x < data.length; x++) {
                    $("#drpAssitedBy").append('<option  id="' + data[x].userId + '" value="' + data[x].userId + '">' + data[x].userName + '</option>');
                }
            },
            error: function (data) {

            }

        })
    }

function populateCounties() {
    var clientId = $("#drpClients :selected").val();

    var model = {
        ClientId: clientId
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetCountries",
        data: model,
        dataType: "json",
        success: function (data) {
            $("#drpCountries").empty();
            $("#drpCountries").append('<option  value="-1" > -- Select A Country -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
            }

        },
        error: function (data) {



        }

    })
}

function populateAllCounties() {
   

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetAllCountries",        
        dataType: "json",
        success: function (data) {
            $("#drpCountries").empty();
            $("#drpCountries").append('<option  value="-1" > -- Select A Country -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
            }

        },
        error: function (data) {



        }

    })
}

function populateDepots() {
    var countryId = $("#drpCountries :selected").val();
    var clientId = $("#drpClients :selected").val();

    var model = {
        CountryId: countryId,
        clientId: clientId,
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetDepotsByCountryclient",
        data: model,
        dataType: "json",
        success: function (data) {

            $("#drpDepots").empty();           
            for (var x = 0; x < data.length; x++) {
                $("#drpDepots").append('<option  id="' + data[x].depotId + '" value="' + data[x].depotCode + '">' + data[x].depotCode + '</option>');
            }
            $('#drpClients').select2({
               // minimumResultsForSearch: 0
            });
        },
        error: function (data) {

        }

    })
}

function populateProducts() {
    var depotId = $("#drpDepots :selected").val();
    var clientId = $("#drpClients :selected").val();
    var countryId = $("#drpCountries :selected").text();

    var model = {
        depotId: depotId,
        clientId: clientId,
        countryId: countryId
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetProductsByClient",
        data: model,
        dataType: "json",
        success: function (data) {

            $("#drpProducts").empty();
            $("#drpProducts").append('<option  value="-1" > -- Select A Product -- </option>');
            for (var x = 0; x < data.length; x++) {
                $("#drpProducts").append('<option  id="' + data[x].productId + '" value="' + data[x].productId + '">' + data[x].productName + '</option>');
            }

        },
        error: function (data) {



        }

    })
}


function calculateTime() {
    startTime = $('#txtStartTime').val();
    endTime = $('#txtEndDate').val();
    if (startTime === '') {

        $('#txtStartTime').css('border', '2px red solid');
        return false;
    }
    else {

        $("#drpClients").css('border', '2px transparent solid');
        //create date format          
       
        var startTimeVal = moment(startTime, "HH:mm a");
        var endTimeVal = moment(endTime, "HH:mm a");
        var duration = moment.duration(endTimeVal.diff(startTimeVal));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes()) - hours * 60;    
        var result = hours + ' hours and ' + minutes + ' minutes.';      
        $('#txtTimeSpent').val(result);        
    }
}

function validate() {

    if ($("#drpClients :selected").val() === '-1') {
        $("#drpClients").css('border', '2px red solid');
        return false;
    } else {
        $("#drpClients").css('border', '1px #c9c8c8  solid');
    }

    if ($("#drpCountries :selected").val() === '-1') {
        $("#drpCountries").css('border', '2px red solid');
        return false;
    } else {
        $("#drpCountries").css('border', '1px #c9c8c8  solid');
    }

    if ($("#drpDepots :selected").val() === '-1') {
        $("#drpDepots").css('border', '2px red solid');
        return false;
    } else {
        $("#drpDepots").css('border', '1px #c9c8c8  solid');
    }

    if ($("#txtRequestorName").val() === '') {
        $("#txtRequestorName").css('border', '2px red solid');
        return false;
    } else {
        $("#txtRequestorName").css('border', '1px #c9c8c8  solid');
    }

    if ($("#drpProducts :selected").val() === '-1') {
        $("#drpProducts").css('border', '2px red solid');
        return false;
    } else {
        $("#drpProducts").css('border', '1px #c9c8c8  solid');
    }

    if ($("#txtDate").val() === '') {
        $("#txtDate").css('border', '2px red solid');
        return false;
    } else {
        $("#txtDate").css('border', '1px #c9c8c8  solid');
    }

    if ($("#txtStartTime").val() === '') {
        $("#txtStartTime").css('border', '2px red solid');
        return false;
    } else {
        $("#txtStartTime").css('border', '1px #c9c8c8  solid');
    }

    if ($("#txtDescription").val() === '') {
        $("#txtDescription").css('border', '2px red solid');
        return false;
    } else {
        $("#txtDescription").css('border', '1px #c9c8c8  solid');
    }

    if ($("#txtResolutionDesc").val() === '') {
        $("#txtResolutionDesc").css('border', '2px red solid');
        return false;
    } else {
        $("#txtResolutionDesc").css('border', '1px #c9c8c8  solid');
    }

    if ($("#drpValidForBilling :selected").val() === '-1') {
        $("#drpValidForBilling").css('border', '2px red solid');
        return false;
    } else {
        $("#drpValidForBilling").css('border', '1px #c9c8c8 solid');
    }

    if ($("#drpValidForBilling :selected").val() === 'false') {
        if ($("#txtReason").val() === '') {
            $("#txtReason").css('border', '2px red solid');
            return false;
        } else {
            $("#txtReason").css('border', '1px transparent solid');
        }
    }


    if ($("#txtEndDate").val() === '') {
        $("#txtEndDate").css('border', '2px red solid');
        return false;
    } else {
        $("#txtEndDate").css('border', '1px transparent solid');
    }

    return true;
}


function reasonForBilling() {

    if ($("#drpValidForBilling :selected").val() === 'true') {
        $("#txtReason").attr('disabled', 'disabled');
        return false;
    } else {
        $("#txtReason").removeAttr('disabled');
    }

}