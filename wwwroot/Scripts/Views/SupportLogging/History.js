﻿var supportCaseTable;
$(document).ready(function () {

    setDatefilters();
    initDataTable();
});

function setDatefilters() {
    var startDate = new Date();
    startDate = startDate.toISOString().split('T')[0];

    var endDate = new Date();
    endDate = endDate.toISOString().split('T')[0];
    $('#txtStartDate').val(startDate);

    var startDateValues = $('#txtStartDate').val().split('-');
    var startDateValues = startDateValues[1] + '/' + startDateValues[2] + '/' + startDateValues[0];
    startDate = startDateValues;
    $('#txtStartDate').val(startDate);

    $('#txtEndDate').val(endDate);
    var endDateValues = $('#txtEndDate').val().split('-');
    var endDateValues = endDateValues[1] + '/' + endDateValues[2] + '/' + endDateValues[0];
    endDate = endDateValues;
    $('#txtEndDate').val(endDate);
}

function initDataTable() {


    supportCaseTable = $('#supportCaseTable').DataTable({
        //"bPaginate": !SiteDataTableSettings.IsNoTablePagingPath,
        pagingType: 'full_numbers',
        columns: [
            {
                'title': 'Date',
                'data': 'date',
                //render: function (data, type, row) {
                //    var dt = new Date(row.Date);
                //    return type === 'sort' ? dt : moment(dt).format('DD/MM/YYYY');
                //}
            },
            {
                'title': 'Total Time',
                'data': 'timeSpentOnSupport'
            }, {
                'title': 'Client Name',
                'data': 'clientName'
            }, {
                'title': 'Country',
                'data': 'country'
            }, {
                'title': 'Depot Code',
                'data': 'depotCode'
            }, {
                'title': 'Product Name',
                'data': 'productName'
            }, {
                'title': 'Issue Description',
                'data': 'issueDescription'
            },
            {
                'title': 'Resolution Desc',
                'data': 'resolutionDesc'
            }, {
                'title': 'Valid For Billing',
                render: function (data, type, row) {
                    return '<label class="badge badge-' + (row.validForBilling ? 'success' : 'info') + '">' + row.validForBilling + '</label>';
                }
            },
            {
                'title': 'Reason',
                'data': 'reasonForNotBilling'
            },
            {
                'title': 'Assited By',
                'data': 'assitedBy'
            },
            {
                'title': 'Edit',
                render: function (data, type, row) {
                    return '<button id="' + row.id + '" onclick="updateSupportCase(' + row.id + ')"; return false;" class="btn btn-outline-primary">View</button>';
                }
            }
        ],
        columnDefs: [
            { type: 'date', targets: 0 },

        ],
        ajax: {
            method: 'GET',
            url: '/SupportLogging/GetSuportCases',
            data: function (d) {
                d.toDate = $('#txtEndDate').val(),
                    d.fromDate = $('#txtStartDate').val()
            }

        }
    });

    $('#supportCaseTable').each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });
};

function updateSupportCase(supportCase) {

    var id = parseInt(supportCase);
    $('#hdnId').val(id);
    var data = { 'supportCaseId': id }

    $.ajax({
        method: "GET",
        url: '/SupportLogging/EditSupportCase',
        dataType: "json",
        data: data,
        cache: false
    })
        .done(function (data) {

            $('#divEditSupportCase').css('display', 'block');

            $("#drpClients").append('<option  id="' + data.clientName + '" value="' + data.clientName + '">' + data.clientName + '</option>');
            $("#drpClients :selected").text(data.clientName);

            $("#drpCountries").append('<option  id="' + data.country + '" value="' + data.country + '">' + data.country + '</option>');
            $("#drpCountries :selected").text(data.country);

            $("#drpDepots").val(data.depotCode);

            $("#txtRequestorName").val(data.requestorName);

            $("#drpProducts").append('<option  id="' + data.productName + '" value="' + data.productName + '">' + data.productName + '</option>');
            $("#drpProducts :selected").text(data.productName);

            $("#txtDate").val(data.date);
            $("#txtStartTime").val(data.startTime);
            $("#txtDescription").val(data.issueDescription);
            $("#txtResolutionDesc").val(data.resolutionDesc);
            $("#drpValidForBilling :selected").val(data.validForBilling);
            $("#txtReason").val(data.reasonForNotBilling);

            $("#drpAssitedBy").append('<option  id="' + data.assitedBy + '" value="' + data.assitedBy + '">' + data.assitedBy + '</option>');
            $("#drpAssitedBy :selected").text(data.assitedBy);

            $("#txtEndTime").val(data.endTime);
            $("#txtTimeSpent").val(data.timeSpentOnSupport);

            $('#supportRef').text('Support Ref: ' + data.supportRef);


        })
        .fail(function (ajaxRequest, status, error) {

        })
        .always(function () {

        });
};

function reloadSupportCase(data, callback, settings) {

    $.ajax({
        type: 'GET',
        url: '/SupportLogging/GetSuportCases',
        dataType: "json",
        success: function (data) {
            if (callback) {
                callback(data);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('request failed');
        }
    });
}

function redirectToLogin() {
    // console.log('Going to login page, session expired');
    window.location.href = '/';
};

function setBusy(isBusy, msg) {
    // console.log('IsBusy: ' + isBusy);
    if (isBusy) {
        var spinner = $('<img/>').attr('id', 'spinner')
            .attr('src', 'Content/Images/loading.gif')
            .attr('alt', 'Busy loading editor please wait...');
        var message = $('<p/>').attr('id', 'status').attr('class', 'well well-sm').attr('style', 'background-color:#f9f9f9;border-color:#ddd;')
            .text(' ' + msg + ' loading, please wait...');

        $("#manageTask").empty().append(message.prepend(spinner));
    } else {
        $("#spinner").remove();
    }
};

function getClients() {

    $.ajax({
        type: "GET",
        url: "/SupportLogging/GetClients",
        dataType: "json",
        success: function (data) {

            $("#drpClients option").empty();
            for (var x = 0; x < data.length; x++) {
                $("#drpClients").append('<option  id="' + data[x].clientId + '" value="' + data[x].clientId + '">' + data[x].name + '</option>');
            }
        },
        error: function (data) {



        }

    })
}

function populateCounties() {
    var clientId = $("#drpClients :selected").val();

    var model = {
        ClientId: clientId
    };

    $.ajax({
        type: "POST",
        url: "/SupportLogging/GetCountries",
        data: model,
        dataType: "json",
        success: function (data) {

            $("#drpCountries option").empty();
            for (var x = 0; x < data.length; x++) {
                $("#drpCountries").append('<option  id="' + data[x].countryId + '" value="' + data[x].countryId + '">' + data[x].countryName + '</option>');
            }

        },
        error: function (data) {



        }

    })
}

function saveSupportCase() {

    isModalValid = validate();

    if (isModalValid) {

        var client = $("#drpClients :selected").text();
        var country = $("#drpCountries :selected").text();
        var depot = $("#drpDepots :selected").text();
        var reuestorName = $("#txtRequestorName").val();
        var products = $("#drpProducts :selected").text();
        var date = $("#txtDate").val();
        var startTime = $("#txtStartTime").val();
        var description = $("#txtDescription").val();
        var resolutionDesc = $("#txtResolutionDesc").val();
        var validForBilling = $("#drpValidForBilling :selected").val();
        var reason = $("#txtReason").val();
        var assitedBy = $("#drpAssitedBy :selected").text();
        var endDate = $("#txtEndTime").val();
        var timeSpent = $("#txtTimeSpent").val();
        var id = parseInt($('#hdnId').val());
        var reasonforEditing = $("#txtReasonForUpdate").val();

        var model = {
            Id: id, ClientName: client, Country: country, DepotCode: depot, RequestorName: reuestorName,
            ProductName: products, Date: date, StartTime: startTime, IssueDescription: description,
            ResolutionDesc: resolutionDesc, ValidForBilling: validForBilling, ReasonForNotBilling: reason, AssitedBy: assitedBy,
            EndTime: endDate, TimeSpentOnSupport: timeSpent, ResonForEditing: reasonforEditing,
        };

        $.ajax({
            type: "POST",
            url: "/SupportLogging/EditSupportCase",
            data: model,
            dataType: "json",
            success: function (data) {

                $('#divEditSupportCase').css('display', 'none');

                swal({
                    title: 'Success!',
                    text: 'You have successfully updated a suppoert case',
                    icon: 'success',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"
                    }
                }).then(function () {
                    supportCaseTable.clear().draw();
                    supportCaseTable.ajax.reload();
                });

            },
            error: function (data) {

                if (data.isSuccess == false) {
                    alert(data.errorMsg);
                }

            }

        })
    }
}

function loadSupportCase() {

    supportCaseTable.clear().draw();
    supportCaseTable.ajax.reload();
}

function cancel() {
    $('#divEditSupportCase').css('display', 'none');
}

function validate() {


    if ($("#txtRequestorName").val() === '') {
        $("#txtRequestorName").css('border', '2px red solid');
        return false;
    } else {
        $("#txtRequestorName").css('border', '1px #c9c8c8 solid');
    }

    if ($("#txtDate").val() === '') {
        $("#txtDate").css('border', '2px red solid');
        return false;
    } else {
        $("#txtDate").css('border', '1px #c9c8c8 solid');
    }

    if ($("#txtStartTime").val() === '') {
        $("#txtStartTime").css('border', '2px red solid');
        return false;
    } else {
        $("#txtStartTime").css('border', '1px #c9c8c8 solid');
    }

    if ($("#txtDescription").val() === '') {
        $("#txtDescription").css('border', '2px red solid');
        return false;
    } else {
        $("#txtDescription").css('border', '1px #c9c8c8 solid');
    }

    if ($("#txtResolutionDesc").val() === '') {
        $("#txtResolutionDesc").css('border', '2px red solid');
        return false;
    } else {
        $("#txtResolutionDesc").css('border', '1px #c9c8c8 solid');
    }

    if ($("#drpValidForBilling :selected").val() === '-1') {
        $("#drpValidForBilling").css('border', '2px red solid');
        return false;
    } else {
        $("#drpValidForBilling").css('border', '1px #c9c8c8 solid');
    }

    if ($("#drpValidForBilling :selected").val() === 'false') {
        if ($("#txtReason").val() === '') {
            $("#txtReason").css('border', '2px red solid');
            return false;
        } else {
            $("#txtReason").css('border', '1px #c9c8c8 solid');
        }
    }


    if ($("#txtEndTime").val() === '') {
        $("#txtEndTime").css('border', '2px red solid');
        return false;
    } else {
        $("#txtEndTime").css('border', '1px #c9c8c8 solid');
    }

    if ($("#txtReasonForUpdate").val() === '') {
        $("#txtReasonForUpdate").css('border', '2px red solid');
        return false;
    } else {
        $("#txtReasonForUpdate").css('border', '1px #c9c8c8 solid');
    }

    return true;
}

function reasonForBilling() {

    if ($("#drpValidForBilling :selected").val() === 'true') {
        $("#txtReason").attr('disabled', 'disabled');
        return false;
    } else {
        $("#txtReason").removeAttr('disabled');
    }

}

function calculateTime() {
    startTime = $('#txtStartTime').val();
    endTime = $('#txtEndTime').val();
    if (startTime === '') {

        $('#txtStartTime').css('border', '2px red solid');
        return false;
    }
    else {

        $("#drpClients").css('border', '2px transparent solid');
        //create date format          
        var timeStart = new Date("01/01/2007 " + startTime).getHours();
        var timeEnd = new Date("01/01/2007 " + endTime).getHours();

        var hourDiff = timeEnd - timeStart;

        $('#txtTimeSpent').val(hourDiff);
    }
}