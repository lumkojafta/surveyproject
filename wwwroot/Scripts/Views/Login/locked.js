﻿$(document).ready(function () {

});

function login() {
    var name = $('#name').val();
    var pwd = $('#password').val();

    var password = btoa(pwd);

    var validate = validateLogin();
    if (validate) {
        $.ajax({
            method: "GET",
            url: '/Login/Login',
            contentType: "application/json; charset=utf-8",
            data: {
                username: name,
                password: password,
            },
            cache: false
        })
            .done(function (data) {
                var url = window.location.href;
                if (data === 'Invalid Username Or Password') {
                    $('#divPasswordError').css('display', 'block');
                    return false;
                }
                if (url.toLowerCase().includes("login/locked")) {
                    url = url.toLowerCase().replace("login/locked", "Home/Wizard");
                    console.log(url);                    
                }
                window.location.href = url;
            })
            .fail(function (ajaxRequest, status, error) {
                console.warn('ajaxRequest: ', ajaxRequest);
                console.warn('status: ', status);
                console.warn('error: ', error);
            });
    }
}



function validateLogin() {
    if ($('#name').val() === '') {
        $('#name').parent().css('border', 'red 2px solid');
        return false;

    } else {
        $('#name').parent().css('border', 'transparent 0px solid');

    }


    if ($('#password').val() === '') {
        $('#password').parent().css('border', 'red 2px solid');
        return false;
    }
    else {
        $('#password').parent().css('border', 'transparent 0px solid');

    }
    return true;
}

function showPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}