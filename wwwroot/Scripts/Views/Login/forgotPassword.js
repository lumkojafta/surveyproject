﻿$(document).ready(function () {

});

function getResetPassword() {
    var email = $('#email').val();
    var url = 'https://'+location.host;

    var validate = validateForm();
    if (validate) {
        $.ajax({
            method: "GET",
            url: '/Login/ResetUserPassword',
            contentType: "application/json; charset=utf-8",
            data: {
                email: email,
                url: url
            },
            cache: false
        })
            .done(function (data) {
                swal({
                    title: 'Successfull!',
                    text: 'Please check your email for the reset link.',
                    icon: 'success',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"                        
                    }
                })

            })
            .fail(function (ajaxRequest, status, error) {
                console.warn('ajaxRequest: ', ajaxRequest);
                console.warn('status: ', status);
                console.warn('error: ', error);
            });
    }
}

function validateForm() {
    if ($('#email').val() === '') {
        $('#email').parent().css('border', 'red 2px solid');
        return false;

    } else {
        $('#email').parent().css('border', 'transparent 0px solid');

    }
    return true;
}