﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupportDash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportDash.Controllers
{
    public class ReportsController : Controller
    {
        private SupportDashboardContext db = new SupportDashboardContext();
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ClientReport()
        {
            return View();
        }

        public IActionResult UserReporting()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetSuportCases(DateTime fromDate, DateTime toDate, string client, string country, string depot, string reportType)
        {
            try
            {
                var roleId = HttpContext.Session.GetInt32("RoleId");
                var createdBy = HttpContext.Session.GetString("Email").Replace("\"", "");

                var jsonData = db.SupportCases.Where(x => (x.Date >= fromDate && x.Date <= toDate) && (x.ClientName == client && x.Country == country && x.DepotCode == depot)).ToList();
                return Json(new { data = jsonData });

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpGet]
        public JsonResult GetUserReport(int userId, int month)
        {
            try
            {
                if (userId > 0 && month > 0)
                {
                    var roleId = HttpContext.Session.GetInt32("RoleId");
                    var createdBy = HttpContext.Session.GetString("Email").Replace("\"", "");

                    var userName = db.Users.Where(x => x.UserId == userId).FirstOrDefault().UserName;

                    var jsonData = db.SupportCases.Where(x => x.AssitedBy == userName && x.Date.Month == month).ToList();
                    return Json(new { data = jsonData });

                }
                else
                {
                    var jsonData = new List<SupportCase>();
                    return Json(new { data = jsonData });
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}
