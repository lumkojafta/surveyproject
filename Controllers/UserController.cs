﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SupportDash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SupportDash.Controllers
{
    public class UserController : Controller
    {
        private SupportDashboardContext db = new SupportDashboardContext();
        public IActionResult UserManagement()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            try
            {
                var items = (from ro in db.Roles
                             join us in db.Users on ro.RoleId equals us.RoleId
                             select new
                             {
                                 us.UserId,
                                 ro.RoleName,
                                 us.Name,
                                 us.UserName,
                                 us.EmailAddress,
                                 us.Surname,                                 
                                 us.Password
                             }).ToList();
                return Json(items);
            }
            catch (Exception ex) {
                return Json(ex.Message);
            }
        }

        [HttpGet]
        public JsonResult EditUser(int userId)
        {
            try
            {
                var jsonData = db.Users.Where(x => x.UserId == userId).FirstOrDefault();
                var roles = db.Roles.ToList();              
                var userView = new UserView();
                {
                    userView.UserId = jsonData.UserId;
                    userView.RoleId = jsonData.RoleId;
                    userView.UserName = jsonData.UserName;
                    userView.Name = jsonData.Name;
                    userView.Surname = jsonData.Surname;
                    userView.Password = jsonData.Password;
                    userView.EmailAddress = jsonData.EmailAddress;                   
                    userView.RoleName = jsonData.Role.RoleName;                 
                }
               
                return Json(userView);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpDelete]
        public JsonResult DeleteUser(int userId)
        {
            try
            {
                var jsonData = db.Users.Where(x => x.UserId == userId).FirstOrDefault();
                db.Remove(jsonData);
                db.SaveChanges();   
                jsonData = new User();
           
            }           
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json("Record deleted Succesfully");
        }

        [HttpGet]
        public JsonResult GetRoles()
        {
            try
            {
                var jsonData = db.Roles.ToList();

                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult SaveUser(UserView UserData)
        {
            var isSuccess = true;
            var errorMsg = "";

            try
            {
                using (var db = new SupportDashboardContext())
                {
                    var roleItems = db.Roles.Where(r => r.RoleId == UserData.RoleId).FirstOrDefault();
                    
                        var roleItem = new Role();                        
                        roleItem.RoleName = UserData.RoleName;
                        //.Description = "BoSS";                        
                    
                
                var item = db.Users.Where(u => u.UserId == UserData.UserId).FirstOrDefault();
                                       
                  if (item == null)
                   {
                        var userItem = new User();
                        userItem.Name = UserData.Name;
                        userItem.UserName = UserData.UserName;
                        userItem.Surname = UserData.Surname;
                        userItem.EmailAddress = UserData.EmailAddress;
                        userItem.Password = UserData.Password;                        
                        userItem.RoleId = UserData.RoleId;
                     
                        db.Users.Add(userItem);
                        db.SaveChanges();                       
                   }
                else {
                    int roleId = Convert.ToInt32(UserData.RoleName);
                        
                    //UserData.RoleName = item.Role.RoleName;
                    Role usersRoles = db.Roles.Where(x => x.RoleId == roleId).FirstOrDefault();
                    item.Name = UserData.Name;
                    item.UserName = UserData.UserName;
                    item.Surname = UserData.Surname;
                    item.EmailAddress = UserData.EmailAddress;
                    item.Password = UserData.Password;     
                    item.RoleId = usersRoles.RoleId;
                    db.Users.Add(item);
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
                };
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            var jsonData = new { isSuccess, errorMsg };
            return Json(jsonData);
        }

    }
}
