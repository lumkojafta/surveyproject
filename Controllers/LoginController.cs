﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupportDash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportDash.Controllers
{
    public class LoginController : Controller
    {
        private SupportDashboardContext db = new SupportDashboardContext();
        public IActionResult Index()
        {
            HttpContext.Session.Remove("UserId");
            HttpContext.Session.Remove("Username");
            HttpContext.Session.Remove("Email");
            HttpContext.Session.Remove("RoleId");

            return View();
        }

        public JsonResult Login(string email, string password)
        {
            try
            {
                User user = new User();
                user = db.Users.Where(x => x.EmailAddress == email).FirstOrDefault();
                if (user != null)
                {
                    if (user.Password == password) { 
                    int userId = Convert.ToInt32(user.UserId);
                        HttpContext.Session.SetInt32("UserId", userId);
                        HttpContext.Session.SetString("Email", user.EmailAddress);
                        HttpContext.Session.SetInt32("RoleId", user.RoleId);

                        return Json(user);
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return Json(msg);
            }

            return Json("Invalid Username Or Password");
        }
    }
}
