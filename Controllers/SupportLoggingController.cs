﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SupportDash.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace SupportDash.Controllers
{
    public class SupportLoggingController : Controller
    {
        private SupportDashboardContext db = new SupportDashboardContext();

        public IActionResult SupportCase()
        {
            return View();
        }

        public IActionResult History()
        {
            return View();
        }
        public IActionResult LandingPage()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetSuportCases(DateTime fromDate, DateTime toDate)
        {
            try
            {
                var roleId = HttpContext.Session.GetInt32("RoleId");
                var createdBy = HttpContext.Session.GetString("Email").Replace("\"", "");
                List<SuuportCaseViewModel> suuportCaseViewModels = new List<SuuportCaseViewModel>();
                if (roleId == 1)
                {
                    var jsonData = db.SupportCases.Where(x => x.Date >= fromDate && x.Date <= toDate).ToList();

                    foreach (var item in jsonData)
                    {
                        var suuportCaseViewModel = new SuuportCaseViewModel();

                        suuportCaseViewModel.Date = item.Date.ToString("yyyy-MM-dd");
                        suuportCaseViewModel.TimeSpentOnSupport = item.TimeSpentOnSupport;
                        suuportCaseViewModel.Id = item.Id;
                        suuportCaseViewModel.ClientName = item.ClientName;
                        suuportCaseViewModel.Country = item.Country;
                        suuportCaseViewModel.DepotCode = item.DepotCode;
                        suuportCaseViewModel.ProductName = item.ProductName;
                        suuportCaseViewModel.IssueDescription = item.IssueDescription;
                        suuportCaseViewModel.ResolutionDesc = item.ResolutionDesc;
                        suuportCaseViewModel.ValidForBilling = item.ValidForBilling;
                        suuportCaseViewModel.AssitedBy = item.AssitedBy;
                        suuportCaseViewModel.ReasonForNotBilling = item.ReasonForNotBilling;

                        suuportCaseViewModels.Add(suuportCaseViewModel);

                    }

                    return Json(new { data = suuportCaseViewModels });
                }
                else
                {
                    var jsonData = db.SupportCases.Where(x => (x.CreatedBy == createdBy) && (x.Date >= fromDate && x.Date <= toDate)).ToList();


                    foreach (var item in jsonData)
                    {
                        var suuportCaseViewModel = new SuuportCaseViewModel();

                        suuportCaseViewModel.Date = item.Date.ToString("yyyy-MM-dd");
                        suuportCaseViewModel.Id = item.Id;
                        suuportCaseViewModel.TimeSpentOnSupport = item.TimeSpentOnSupport;
                        suuportCaseViewModel.ClientName = item.ClientName;
                        suuportCaseViewModel.Country = item.Country;
                        suuportCaseViewModel.DepotCode = item.DepotCode;
                        suuportCaseViewModel.ProductName = item.ProductName;
                        suuportCaseViewModel.IssueDescription = item.IssueDescription;
                        suuportCaseViewModel.ResolutionDesc = item.ResolutionDesc;
                        suuportCaseViewModel.ValidForBilling = item.ValidForBilling;
                        suuportCaseViewModel.AssitedBy = item.AssitedBy;
                        suuportCaseViewModel.ReasonForNotBilling = item.ReasonForNotBilling;

                        suuportCaseViewModels.Add(suuportCaseViewModel);

                    }

                    return Json(new { data = suuportCaseViewModels });
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult SaveUser(UserView UserData)
        {
            var isSuccess = true;
            var errorMsg = "";

            try
            {
                using (var db = new SupportDashboardContext())
                {
                    var roleItems = db.Roles.Where(r => r.RoleId == UserData.RoleId).FirstOrDefault();

                    var roleItem = new Role();
                    roleItem.RoleName = UserData.RoleName;
                    //.Description = "BoSS";                        


                    var item = db.Users.Where(u => u.UserId == UserData.UserId).FirstOrDefault();

                    if (item == null)
                    {
                        var userItem = new User();
                        userItem.Name = UserData.Name;
                        userItem.UserName = UserData.UserName;
                        userItem.Surname = UserData.Surname;
                        userItem.EmailAddress = UserData.EmailAddress;
                        userItem.Password = UserData.Password;
                        userItem.RoleId = UserData.RoleId;

                        db.Users.Add(userItem);
                        db.SaveChanges();
                    }
                    else
                    {
                        int roleId = Convert.ToInt32(UserData.RoleName);

                        //UserData.RoleName = item.Role.RoleName;
                        Role usersRoles = db.Roles.Where(x => x.RoleId == roleId).FirstOrDefault();
                        item.Name = UserData.Name;
                        item.UserName = UserData.UserName;
                        item.Surname = UserData.Surname;
                        item.EmailAddress = UserData.EmailAddress;
                        item.Password = UserData.Password;
                        item.RoleId = usersRoles.RoleId;
                        db.Users.Add(item);
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            var jsonData = new { isSuccess, errorMsg };
            return Json(jsonData);
        }



        [HttpPost]
        public JsonResult SaveSupportCase(SupportCase supportCase)
        {

            var isSuccess = true;
            var errorMsg = "";

            List<string> depotCodes = supportCase.DepotCode.Split(",").ToList();
            supportCase.DepotCode = "";
            foreach (var item in depotCodes)
            {
                
                if (depotCodes.Count == 1) { supportCase.DepotCode += item + " "; }
                else if (depotCodes.Count > 1) { supportCase.DepotCode += item + " | "; }
            }

            var client = supportCase.ClientName.Substring(0, 2);
            var countryStr = supportCase.Country.Substring(0, 2);
            var deopt = supportCase.DepotCode.Substring(0, 2);

            var rng = new Random();
            int value = rng.Next(100000);
            string text = Convert.ToString(value).Substring(0, 3);

            supportCase.SupportRef = client + countryStr + deopt + text;
            supportCase.CreatedBy = HttpContext.Session.GetString("Email").Replace("\"", "");

            db.SupportCases.Add(supportCase);
            db.SaveChanges();

            var jsonData = new { isSuccess, errorMsg };

            return Json(jsonData);

        }

        [HttpPost]
        public JsonResult SaveCase(SupportCase supportCase)
        {

            var isSuccess = true;
            var errorMsg = "";

            List<string> depotCodes = supportCase.DepotCode.Split(",").ToList();
            supportCase.DepotCode = "";
            foreach (var item in depotCodes)
            {

                if (depotCodes.Count == 1) { supportCase.DepotCode += item + " "; }
                else if (depotCodes.Count > 1) { supportCase.DepotCode += item + " | "; }
            }

            var client = supportCase.ClientName.Substring(0, 2);
            var countryStr = supportCase.Country.Substring(0, 2);
            var deopt = supportCase.DepotCode.Substring(0, 2);

            var rng = new Random();
            int value = rng.Next(100000);
            string text = Convert.ToString(value).Substring(0, 3);

            supportCase.SupportRef = client + countryStr + deopt + text;
            supportCase.CreatedBy = HttpContext.Session.GetString("Email").Replace("\"", "");

            db.SupportCases.Add(supportCase);
            db.SaveChanges();

            var jsonData = new { isSuccess, errorMsg };

            return Json(jsonData);

        }

        [HttpPost]
        public JsonResult EditSupportCase(SupportCase supportCase)
        {

            var isSuccess = true;
            var errorMsg = "";

            var updatedSupportCase = db.SupportCases.Where(x => x.Id == supportCase.Id).FirstOrDefault();

            if (updatedSupportCase != null)
            {
                updatedSupportCase.ClientName = supportCase.ClientName;
                updatedSupportCase.Country = supportCase.Country;
                updatedSupportCase.RequestorName = supportCase.RequestorName;
                updatedSupportCase.ProductName = supportCase.ProductName;
                updatedSupportCase.Date = supportCase.Date;
                updatedSupportCase.StartTime = supportCase.StartTime;
                updatedSupportCase.IssueDescription = supportCase.IssueDescription;
                updatedSupportCase.ValidForBilling = supportCase.ValidForBilling;
                updatedSupportCase.ReasonForNotBilling = supportCase.ReasonForNotBilling;
                updatedSupportCase.AssitedBy = supportCase.AssitedBy;
                updatedSupportCase.EndTime = supportCase.EndTime;
                updatedSupportCase.TimeSpentOnSupport = supportCase.TimeSpentOnSupport;
                updatedSupportCase.ResolutionDesc = supportCase.ResolutionDesc;
                updatedSupportCase.ResonForEditing = supportCase.ResonForEditing;

                db.SupportCases.Attach(updatedSupportCase);
                db.Entry(updatedSupportCase).State = EntityState.Modified;
                db.SaveChanges();

                var jsonData = new { isSuccess, errorMsg };

                return Json(jsonData);
            }
            else
            {
                List<string> depotCodes = supportCase.DepotCode.Split(",").ToList();
                supportCase.DepotCode = "";
                foreach (var item in depotCodes)
                {

                    if (depotCodes.Count == 1) { supportCase.DepotCode += item + " "; }
                    else if (depotCodes.Count > 1) { supportCase.DepotCode += item + " | "; }
                }

                var client = supportCase.ClientName.Substring(0, 2);
                var countryStr = supportCase.Country.Substring(0, 2);
                var deopt = supportCase.DepotCode.Substring(0, 2);

                var rng = new Random();
                int value = rng.Next(100000);
                string text = Convert.ToString(value).Substring(0, 3);

                supportCase.SupportRef = client + countryStr + deopt + text;
                supportCase.CreatedBy = HttpContext.Session.GetString("Email").Replace("\"", "");

                db.SupportCases.Add(supportCase);
                db.SaveChanges();

                var jsonData = new { isSuccess, errorMsg };

                return Json(jsonData);
            }

        }



        #region Populate Drop downs

        [HttpGet]
        public JsonResult GetClients()
        {
            try
            {
                var jsonData = db.Clients.ToList();
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetClientsByCountryId(int countryId)
        {
            try
            {
                var countryClients = db.CountryClients.Where(x => x.CountryId == countryId).ToList();
                var jsonData = new List<Client>();

                foreach (var item in countryClients)
                {
                    jsonData.Add(db.Clients.Where(x => x.ClientId == item.ClientId).FirstOrDefault());
                }
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            try
            {
                var jsonData = db.Users.ToList();
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetCountries(int ClientId)
        {
            try
            {
                var countryClients = db.CountryClients.Where(x => x.ClientId == ClientId).ToList();
                var jsonData = new List<Country>();

                foreach (var country in countryClients.Distinct())
                {
                    jsonData.Add(db.Countries.Where(x => x.CountryId == country.CountryId).FirstOrDefault());
                }
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }


        [HttpPost]
        public JsonResult GetAllCountries()
        {
            try
            {
                var jsonData = db.Countries.ToList();

                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult GetCountriesByClientName(string ClientName)
        {
            try
            {
                var ClientId = db.Clients.Where(y => y.Name == ClientName).FirstOrDefault().ClientId;
                var countryClients = db.CountryClients.Where(x => x.ClientId == ClientId).ToList();

                var jsonData = new List<Country>();
                foreach (var item in countryClients)
                {
                    jsonData.Add(db.Countries.Where(x => x.CountryId == item.CountryId).FirstOrDefault());
                }
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetDepots(int CountryId)
        {
            try
            {
                var depotCountries = db.DepotCountries.Where(dc => dc.CountryId == CountryId).ToList();
                var jsonData = new List<Depot>();
                foreach (var item in depotCountries)
                {
                    jsonData.Add(db.Depots.Where(x => x.DepotId == item.DepotId).FirstOrDefault());
                }
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetDepotsByCountryclient(int countryId, int clientId)
        {
            try
            {
                var depotCountries = db.DepotCountries.Where(dc => dc.CountryId == countryId && dc.ClientId == clientId).ToList();
                var jsonData = new List<Depot>();
                foreach (var item in depotCountries.Distinct())
                {
                    jsonData.Add(db.Depots.Where(x => x.DepotId == item.DepotId).FirstOrDefault());
                }
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetProducts(int DepotId)
        {
            try
            {
                var jsonData = db.Products.ToList();
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetProductsByClient(int clientId)
        {
            try
            {
                var clietProduct = db.ClientProducts.Where(cp => cp.ClientId == clientId).ToList();
                List<Product> products = new List<Product>();
                var jsonData = new Product();

                foreach (var item in clietProduct)
                {
                    jsonData = db.Products.Where(p => p.ProductId == item.ProductId).FirstOrDefault();
                    products.Add(jsonData);
                }


                return Json(products);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpGet]
        public JsonResult EditSupportCase(int supportCaseId)
        {
            try
            {
                var jsonData = db.SupportCases.Where(x => x.Id == supportCaseId).FirstOrDefault();
                return Json(jsonData);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion
    }
}
